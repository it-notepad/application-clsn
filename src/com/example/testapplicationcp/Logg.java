package com.example.testapplicationcp;

import android.util.Log;

/**
 * Own class for Logging. Before create Release version, change debug to false
 * @author Timur Gaysin
 *
 */
public class Logg{
	
	private final static boolean debug = true;
	public static int INFO = Log.INFO;
	
	public static void i(String TAG,String message){
		if(debug){
			Log.i(TAG, message);
		}
		
	}
	
	public static void v(String TAG,String message){
		if(debug){
			Log.v(TAG, message);
		}
		
	}

	public static void e(String TAG,String message){
		if(debug){
			Log.e(TAG, message);
		}
		
	}
	
	public static void d(String TAG,String message){
		if(debug){
			Log.d(TAG, message);
		}
		
	}
	
	public static void wtf(String TAG,String message){
		if(debug){
			Log.wtf(TAG, message);
		}
		
	}


}
