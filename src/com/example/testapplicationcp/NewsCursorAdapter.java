package com.example.testapplicationcp;

import java.sql.Date;
import java.text.SimpleDateFormat;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class NewsCursorAdapter extends CursorAdapter {
	
	private static final String TAG = "NewsCursorAdapter:TestApplication";
	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy:HH:mm");

	@SuppressWarnings("deprecation")
	public NewsCursorAdapter(Context context, Cursor c) {
		super(context, c);
	}
	
	public NewsCursorAdapter(Context context, Cursor c,int i) {
		super(context, c,0);
	}
	
	

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		
		 return LayoutInflater.from(context).inflate(R.layout.element_news, parent, false);
	}

	@Override
	public void bindView(View view, final Context context, Cursor cursor) {
		TextView themeView = (TextView) view.findViewById(R.id.theme_textView);
		TextView dateView = (TextView) view.findViewById(R.id.date_textView);
		TextView newsView = (TextView) view.findViewById(R.id.news_textView);
		String themeString = cursor.getString(cursor.getColumnIndexOrThrow(DescriptionDatabase.THEME));
		long dateString = cursor.getLong(cursor.getColumnIndexOrThrow(DescriptionDatabase.DATE));
		String newsString = cursor.getString(cursor.getColumnIndexOrThrow(DescriptionDatabase.TEXT));

		Date date = new Date(dateString);
		
		//set data for components
		themeView.setText(themeString);
		dateView.setText(sdf.format(date));
		newsView.setText(newsString);
		
	}
	
	

}
