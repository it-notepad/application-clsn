package com.example.testapplicationcp;


import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


public class NewsRestRequest {

	//in real Application, please use SESSIONS/TAKEN'S AND SSL
	private final String KEY_ID =  "KEY_ID_23456";
	private final String TAG = "NewsRestRequest:TestApplication";
	//URI rest web service
	private final String URI_REST = "http://it-notepad.com/TestApplication/restnews/news/getNews";
	
	public NewsRestRequest(){
		
	}

	public JSONObject doInBackground() {
		JSONObject objectReturn;
		DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();

	    HttpPost localHttpPost = new HttpPost(URI_REST);
	    try
	    {
	    	JSONObject jsonobj = new JSONObject();
	    	try {
	    		//FICTIONAL session
				jsonobj.put("session", KEY_ID);
			
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
	    	

	    	StringEntity se = new StringEntity(jsonobj.toString());
	    	se.setContentType("application/json;charset=UTF-8");
	    	se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));

	    	localHttpPost.setEntity(se);
	    	//�����
	        HttpResponse localHttpResponse = localDefaultHttpClient.execute(localHttpPost);
	        Logg.i(TAG,"responsecode " + localHttpResponse.getStatusLine().getStatusCode());
	        //return 200 response, if KEY is correct
	        if(localHttpResponse.getStatusLine().getStatusCode()==200){
	        	String string = EntityUtils.toString(localHttpResponse.getEntity());
	        	try {
	        		objectReturn = new JSONObject(string);
					
					return objectReturn;
				} catch (JSONException e) {
					e.printStackTrace();
				}
	        	
	        }else if(localHttpResponse.getStatusLine().getStatusCode()==403){
	        //Handler of auth error
	        }
	    }catch(IOException e){
        	
	    	e.printStackTrace();
	    }
		
		return null;
	}
}
