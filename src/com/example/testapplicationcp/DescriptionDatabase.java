package com.example.testapplicationcp;

import android.net.Uri;

/**
 * 
 * @author Timur Gaysin
 *
 */
public class DescriptionDatabase {
	
	public static final String SIMPLE_AUTHORITY="com.example.testapplicationcp.DescriptionDatabase";
	
	public static final Uri CONTENT_URI = Uri.parse("content://"+SIMPLE_AUTHORITY+"/news");
	
	//MIME type of catalogue fields
	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.news.item";
	
	//MIME type of catalogue fields
	public static final String CONTENT_ROLE_TYPE = "vnd.android.cursor.item/vnd.news.item";
	//default sort - Decrease
	public static final String DEFAULT_SORT_ORDER = "modified DESC";
	
	public static final String ROLE_PATH="news"; 
	/*
	 * fields database
	 */
	public static final String ID = "_id";
	
	public static final String THEME = "theme";
	
	public static final String TEXT = "text";
	
	public static final String DATE = "date";
	/*
	 * status
	 * 0 - news read,
	 * 1 - news unread
	 */
	public static final String STATUS = "status";


}
