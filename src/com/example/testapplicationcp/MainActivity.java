package com.example.testapplicationcp;

import java.util.Random;
import java.util.concurrent.Semaphore;

import org.json.JSONException;
import org.json.JSONObject;




import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

@SuppressWarnings("deprecation")
/**
 * 
 * @author Timur Gaysin
 *
 */
public class MainActivity extends ActionBarActivity {

	//Handler for update news interface
	private Handler handler;
	//Observer Changes to the database
	private NewsContentObserver observer=null;
	//FLAG - updatw news interface
	public static final int NEW_MESSAGE=1;
	private final String TAG = "MainActivity:TestApplication";
	//������� ��� ������������� ������ � ��
	private Semaphore semaphoreDb = new Semaphore(1);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment(this)).commit();
		}
		handler = new Handler(){
	        @Override
	        public void handleMessage(Message msg) {
	        	switch(msg.what){
	        		case NEW_MESSAGE:
	        			//replace fragment
	        			Fragment newFragment = new PlaceholderFragment(getApplicationContext());
	    				FragmentTransaction transaction = getFragmentManager().beginTransaction();

	    			
	    				transaction.replace(R.id.container, newFragment);
	    				transaction.addToBackStack(null);

	    				transaction.commit();
	        			break;
	        	}
	        }
		};
		//initialize of Observer
		if(observer==null)
			observer= new NewsContentObserver(handler);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}
	
	@Override
	public void onStart(){
		super.onStart();
	}
	
	@Override
	public void onPause(){
		super.onPause();
		//unregister content observer
		getContentResolver().unregisterContentObserver(observer);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		//register content observer
		getContentResolver().registerContentObserver(DescriptionDatabase.CONTENT_URI, true,observer );
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
			newRestNewsRequest();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event){ 
	        
	    int action = MotionEventCompat.getActionMasked(event);
	        
	    switch(action) {
	        case (MotionEvent.ACTION_DOWN) :
	            Logg.d(TAG,"Action was DOWN");
	        	
	            return true;
	        
	        default : 
	            return super.onTouchEvent(event);
	    }      
	}
	
	/**
	 * get news in separate Thread
	 */
	private void newRestNewsRequest(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				//get news from owm web-service
				JSONObject object =  new NewsRestRequest().doInBackground();
				String textNews = null;
				String themenews = null;
				long date=0L;
				//If internet is down or our web-service is down, load news from xml-file
				if(object==null){
					//get random news from range of 4 to 1;
					int value = randInt(1, 4);
					date = System.currentTimeMillis();
					switch(value){
					case 1:
						textNews = getApplicationContext().getResources().getString(R.string.text_1);
						themenews = getApplicationContext().getResources().getString(R.string.theme_1);
						break;
					case 2:
						textNews = getApplicationContext().getResources().getString(R.string.text_2);
						themenews = getApplicationContext().getResources().getString(R.string.theme_2);
						break;
					case 3:
						textNews = getApplicationContext().getResources().getString(R.string.text_3);
						themenews = getApplicationContext().getResources().getString(R.string.theme_3);
						break;
					case 4:
						textNews = getApplicationContext().getResources().getString(R.string.text_4);
						themenews = getApplicationContext().getResources().getString(R.string.theme_4);
						break;
					default:
						textNews = getApplicationContext().getResources().getString(R.string.text_1);
						themenews = getApplicationContext().getResources().getString(R.string.theme_1);
						break;
					}
					
				}else{
					try {
						//get data from JSONObject
						themenews=object.getString("theme");
						textNews=object.getString("text");
						date = object.getLong("date");
						
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				insertValuesDb(themenews, textNews, date);
				
			
			}
		}).start();
	}
	
	/**
	 * Insert data into Database
	 * @param theme
	 * @param text
	 * @param date
	 */
	private void insertValuesDb(String theme, String text, long date){
		//Acquire a permit of Semaphore
		try {
			semaphoreDb.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//Create ContentValues object
		ContentValues values = new ContentValues();
		values.put(DescriptionDatabase.THEME, theme);
		values.put(DescriptionDatabase.TEXT, text);
		values.put(DescriptionDatabase.DATE, date);
		//set status of news - 1(unread)
		values.put(DescriptionDatabase.STATUS, 1);
		//insert value into Database
		getContentResolver().insert(DescriptionDatabase.CONTENT_URI, values);
		//release semaphore
		semaphoreDb.release();
		//send empty message with Code - NEW_MESSAGE for our handler
		handler.sendEmptyMessage(NEW_MESSAGE);
	}
	
	/**
	 * Generate random value in the range
	 * @param min - min value of range
	 * @param max - max value of range
	 * @return Random int value  
	 */
	 public static int randInt(int min, int max) {

		  
		    Random rand = new Random();

		    int randomNum = rand.nextInt((max - min) + 1) + min;

		    return randomNum;
		}

	/**
	 * A placeholder fragment containing a listView.
	 */
	public static class PlaceholderFragment extends Fragment {
		
		//listWiew with all news
		private ListView listview;
		private Context context;
		private NewsCursorAdapter adapter;

		public PlaceholderFragment(Context context) {
			this.context = context;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			//get instance of ListView
			listview = (ListView)rootView.findViewById(R.id.listView_news);
			Uri uri = DescriptionDatabase.CONTENT_URI;
			//get All news(read and unread)
			Cursor mCursor = context.getContentResolver().query(uri, null, null, null, DescriptionDatabase.DATE + " DESC");
			//initialize of adapter
			adapter = new NewsCursorAdapter(context, mCursor, 0); 
			//set own Data behind listView
			listview.setAdapter(adapter);
			return rootView;
		}
	}
}
