package com.example.testapplicationcp;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;

/**
 * 
 * @author Timur Gaysin
 *
 */
public class NewsContentProvider extends ContentProvider {

	private static final String DATABASE_NAME = "news.db";
	//PERSPECTIVE VALUE - version of database
	private static final int DATABASE_VERSION = 1;
	private static final String ROLE_TABLE_NAME = "news";
	private static UriMatcher sUriMatcher;
	private static final int NEWS = 1;
	private static final int NEWS_ID = 2;


	private final String TAG = "NewsContentProvider:TestApplication";
	
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	
	static{
		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sUriMatcher.addURI(DescriptionDatabase.SIMPLE_AUTHORITY, DescriptionDatabase.ROLE_PATH, NEWS);
		sUriMatcher.addURI(DescriptionDatabase.SIMPLE_AUTHORITY,
				DescriptionDatabase.ROLE_PATH + "/#", NEWS_ID);
	}
	
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
		case NEWS:
			return DescriptionDatabase.CONTENT_TYPE;
		case NEWS_ID:
			return DescriptionDatabase.CONTENT_ROLE_TYPE;
		default:
			throw new IllegalArgumentException("Unknown role type: " +uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Logg.i(TAG,"insert");
		if (sUriMatcher.match(uri) != NEWS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

		db = dbHelper.getWritableDatabase();
	    long rowID = db.insert(ROLE_TABLE_NAME, null, values);
	    Uri resultUri = ContentUris.withAppendedId(DescriptionDatabase.CONTENT_URI, rowID);
	    // send notify to register ContentObserver
	    getContext().getContentResolver().notifyChange(resultUri, null);
	    return resultUri;
	}

	@Override
	public boolean onCreate() {
		dbHelper = new DatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		int match = sUriMatcher.match(uri);
		
		Cursor c;
		switch (match) {
			case NEWS:
				// get ALL news
				c = db.query(ROLE_TABLE_NAME, projection,
						selection, selectionArgs, null, null, sortOrder);
				c.setNotificationUri(
						getContext().getContentResolver(),
						DescriptionDatabase.CONTENT_URI);
				break;
			case NEWS_ID:
			// get news by he ID
				long news_id = ContentUris.parseId(uri);
				c = db.query(ROLE_TABLE_NAME, projection,
						DescriptionDatabase.ID + " = " + news_id +
						(!TextUtils.isEmpty(selection) ?
						" AND (" + selection + ')' : ""),selectionArgs, null, null, sortOrder);
				c.setNotificationUri(
				getContext().getContentResolver(),
				DescriptionDatabase.CONTENT_URI);
				break;
			default:
				throw new IllegalArgumentException("unsupported uri: " + uri);
				}
				return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		Logg.i(TAG,"update");
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int affected;
		switch (sUriMatcher.match(uri)) {
			case NEWS:
				affected = db.update(ROLE_TABLE_NAME, values,
						selection, selectionArgs);
				break;
			case NEWS_ID:
				String newsId = uri.getPathSegments().get(1);
				affected = db.update(ROLE_TABLE_NAME, values,
						DescriptionDatabase.ID + "=" + newsId
						+ (!TextUtils.isEmpty(selection) ?
						" AND (" + selection + ')' : ""),selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return affected;
	}
	
private static class DatabaseHelper extends SQLiteOpenHelper{
		
		private static final String TAG = "TMS:DatabaseHelper";
		
		public DatabaseHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
		}
		
		public DatabaseHelper(Context context) {
			super(context,DATABASE_NAME, null, 2);
			Logg.i(TAG,"Instantieted new class"+this.getClass());
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			//create database with news
			Logg.i(TAG,"onCreate");
			String qs = "CREATE TABLE "+ROLE_TABLE_NAME + " ("+DescriptionDatabase.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					DescriptionDatabase.THEME + " TEXT, "+DescriptionDatabase.TEXT + " TEXT, "+
					DescriptionDatabase.DATE + " INTEGER, "+DescriptionDatabase.STATUS + " INTEGER );";
			db.execSQL(qs);
			
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			//in future, construction for update version of databases
			//TODO
		}
		
	}

}
