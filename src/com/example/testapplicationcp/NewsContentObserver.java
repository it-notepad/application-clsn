package com.example.testapplicationcp;

import android.database.ContentObserver;
import android.os.Handler;

/**
 * 
 * @author Timur Gaysin
 *
 */
public class NewsContentObserver extends ContentObserver{
	
	private  final String TAG = "NewsContentObserver:TestApplication";

	public NewsContentObserver(Handler handler) {
		super(handler);
	}
	
	@Override
    public boolean deliverSelfNotifications() {
        return true;
    }

    @Override
    public void onChange(boolean selfChange) {
        Logg.d(TAG, "MyContentObserver.onChange("+selfChange+")");
        super.onChange(selfChange);

    }

}
